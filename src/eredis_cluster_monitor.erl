-module(eredis_cluster_monitor).
-behaviour(gen_server).

%% API.
-export([start_link/1]).
-export([get_cluster_slots/1]).

%% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-define(RECONNECTION_LIMIT, 5).
-define(RECONNECT_TIMEOUT, 15000).%% Type definition.

-include("eredis_cluster.hrl").
-record(state, {
    name,
    connection_state = disconnected :: disconnected | connected,
    slots_maps_state = waiting :: waiting | success,

    connection_pid = undefined :: pid(),
    reconnect_counter = 0 :: integer(),
    reconnect_timeout = ?RECONNECT_TIMEOUT :: integer(),

    reconnect_tref = undefined :: undefined | reference(),
    reload_slots_tref = undefined :: undefined | reference(),

    init_node = {} :: tuple(),
    slots_maps = [] :: [#slots_map{}]
}).

%% =============================================================================
%% API.
%% =============================================================================
-spec start_link(tuple()) -> {ok, pid()}.
start_link([{SrvName, _NodeHost, _NodePort} = Args]) ->
    gen_server:start_link({local, SrvName}, ?MODULE, [Args], []).

get_cluster_slots(SrvName) ->
    gen_server:call(SrvName, get_cluster_slots, 5000).

%% ==========================================================================
%% gen_server callbacks
%% ==========================================================================
init([{SrvName, Host, Port}]) ->
    process_flag(trap_exit, true),
    _Msg = erlang:send(self(), connect),
    {ok, #state{name = SrvName, init_node = {Host, Port}}}.

handle_call(get_cluster_slots, _From, #state{connection_state = connected, slots_maps_state = success, slots_maps = SlotsMaps} = State) ->
    {reply, {ok, SlotsMaps}, State};
handle_call(get_cluster_slots, _From, #state{connection_state = connected, slots_maps_state = waiting} = State) ->
    {reply, {error, slots_maps_not_ready_yet}, State};
handle_call(get_cluster_slots, _From, State) ->
    {reply, {error, no_connection}, State};


handle_call(_Request, _From, State) ->
    {reply, ignored, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(connect, State) ->
    {ok, cancel} = safe_timer_cancel(State#state.reconnect_tref),
    NewState = connect(State),
    {noreply, NewState};
handle_info(get_cluster_slots, State) ->
    {ok, cancel} = safe_timer_cancel(State#state.reload_slots_tref),
    NewState =
        case get_cluster_slots_(State) of
            {ok, ClusterSlots} ->
                State#state{slots_maps_state = success, slots_maps = ClusterSlots};
            _ ->
                State#state{slots_maps_state = waiting}
    end,
    {ok, Tref} = timer:send_after(5000, self(), conget_cluster_slots),
    {noreply, NewState#state{reload_slots_tref = Tref}};


%% reconnection repeat
handle_info(reconnection_timeout, State) ->
    error_logger:error_msg("{redis cluster monitor} disconnected, try after ~p~n", [?RECONNECT_TIMEOUT]),
    {ok, ReconnectTref} = timer:send_after(?RECONNECT_TIMEOUT, self(), connect),
    {noreply, State#state{reconnect_tref = ReconnectTref, reconnect_counter = 0}};
%% ERRORS
handle_info({'EXIT', _Pid, Reason}, #state{name = Name, connection_pid = ConnPid} = State) ->
    error_logger:error_msg("{redis cluster monitor} 'EXIT' <PID> ~p, Name ~p reason ~p~n", [_Pid, Name, Reason]),
    catch eredis:stop(ConnPid),
    {ok, ReconnectTref} = timer:send_after(?RECONNECT_TIMEOUT, self(), connect),
    {noreply, State#state{connection_state = disconnected, reconnect_tref = ReconnectTref}};
handle_info({'DOWN', Ref, process, _Pid, Reason}, #state{name = Name, connection_pid = ConPid} = State) ->
    erlang:demonitor(Ref),
    catch eredis:stop(ConPid),
    error_logger:error_msg("{redis cluster monitor} 'DOWN' disconnected node ~p, reason ~p~n", [Name, Reason]),
    {ok, ReconnectTref} = timer:send_after(?RECONNECT_TIMEOUT, self(), connect),
    {noreply, State#state{connection_state = disconnected, reconnect_tref = ReconnectTref}};
handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%% internal Functions
%% =============================================================================
%% @doc Given a slot return the link (Redis instance) to the mapped
%% node.
%% @end
%% =============================================================================
get_cluster_slots_(#state{connection_pid = ConnPid, init_node = HostPortNode} = _State) ->
    try
        RedisResp = eredis:q(ConnPid, ["CLUSTER", "SLOTS"]),
        case parse_resp(RedisResp, HostPortNode) of
            {ok, ClusterInfo} ->
                parse_cluster_slots(ClusterInfo);
            E -> E
        end
    catch
        _:E2 -> E2
    end.


parse_resp({error, <<"ERR unknown command 'CLUSTER'">>}, Node) ->
    {ok, get_cluster_slots_from_single_node(Node)};
parse_resp({error, <<"ERR This instance has cluster support disabled">>}, Node) ->
    {ok, get_cluster_slots_from_single_node(Node)};
parse_resp({ok, _ClusterInfo} = OkReply, _Node) ->
    OkReply;
parse_resp(AnyError, Node) ->
    error_logger:error_msg("{redis cluster monitor} e. = ~p for node = ~p~n", [AnyError, Node]),
    AnyError.


-spec get_cluster_slots_from_single_node({Host:: inet:hostname(), Port::inet:port_number()}) ->
    [[bitstring() | [bitstring()]]].
get_cluster_slots_from_single_node({Host, Port}) ->
    [[<<"0">>, integer_to_binary(?REDIS_CLUSTER_HASH_SLOTS - 1),
    [list_to_binary(Host), integer_to_binary(Port)]]].

-spec parse_cluster_slots([[bitstring() | [bitstring()]]]) -> [#slots_map{}].
parse_cluster_slots(ClusterInfo) ->
    parse_cluster_slots(ClusterInfo, 1, []).

parse_cluster_slots([SlotData | T], Index, Acc) ->
    {StartSlot, EndSlot, Address, Port, NodeId} = parse_cluster_slot(SlotData),
    SlotsMap =
        #slots_map{
            index = Index,
            start_slot = binary_to_integer(StartSlot),
            end_slot = binary_to_integer(EndSlot),
            node = #node{
                address = binary_to_list(Address),
                port = binary_to_integer(Port),
                node_id = NodeId
            }
        },
    parse_cluster_slots(T, Index+1, [SlotsMap | Acc]);
parse_cluster_slots([], _Index, Acc) ->
    {ok, lists:reverse(Acc)}.

%% New version, includes IDs
parse_cluster_slot([StartSlot, EndSlot | [[Address, Port, NodeId | _] | _]]) ->
    {StartSlot, EndSlot, Address, Port, NodeId};
%% Old Vsn
parse_cluster_slot([StartSlot, EndSlot | [[Address, Port | _] | _]]) ->
    {StartSlot, EndSlot, Address, Port, <<>>}.


-spec connect(State0::#state{}) -> State :: #state{}.
connect(#state{name = Name, init_node = {Host, Port} = Node} = State) when State#state.reconnect_counter < ?RECONNECTION_LIMIT ->
    error_logger:info_msg("{redis cluster monitor} ~p connecting to node ~p~n", [Name, Node]),
    case eredis:start_link(Host, Port) of
        {ok, ConnectionPid} ->
            erlang:monitor(process, ConnectionPid),
            error_logger:info_msg("{redis cluster monitor} ~p connected to node ~p~n", [State#state.name, Node]),
            _Msg = erlang:send(self(), get_cluster_slots),
            State#state{connection_state = connected, reconnect_counter = 0, connection_pid = ConnectionPid};
        Error ->
            error_logger:error_msg("{redis cluster monitor} ~p conencting e. = ~p~n", [State#state.name, Error]),
            {ok, Tref} = timer:send_after(5000, self(), connect),
            State#state{connection_state = disconnected, reconnect_counter = State#state.reconnect_counter + 1, reconnect_tref = Tref}
    end;
connect(State) ->
    error_logger:error_msg("{redis cluster monitor} ~p reconnection limit: ~p~n", [State#state.name,  State#state.reconnect_counter]),
    _Msg = erlang:send(self(), reconnection_timeout),
    State.


safe_timer_cancel(undefined) -> {ok, cancel};
safe_timer_cancel({_, Ref} = TimerRef) when is_reference(Ref) ->
    timer:cancel(TimerRef).

