%%%-------------------------------------------------------------------
%%% @author vitalikletsko
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 22. Нояб. 2017 4:38 PM
%%%-------------------------------------------------------------------
-module(eredis_cluster_controller).
-author("vitalikletsko").

-behaviour(gen_server).

%% API
-export([start_link/0]).

-export([get_all_pools/0]).
-export([get_pool_by_slot/1]).
-export([refresh_mapping/1]).


-include("eredis_cluster.hrl").
-define(CHILD(I, Type, Args), {I, {I, start_link, [Args]}, permanent, 5000, Type, [I]}).

-define(RECONNECT_TIMEOUT, 30000).

%% gen_server callbacks
-export([init/1,
	handle_call/3,
	handle_cast/2,
	handle_info/2,
	terminate/2,
	code_change/3]).


-record(state, {
    monitors = [] :: [atom()],
    tref_load_maps = undefined :: timer:tref(),
    init_nodes = [] :: [#node{}],
		pool_name = undefined :: atom(),
		pool_size = 10,
		max_overflow_size = 10,
    slots_maps = [],
    version = 0 :: integer()
}).


%%%===================================================================
%%% API
%%%===================================================================
refresh_mapping(Vsn) ->
	gen_server:call(?MODULE, {reload_slots_map, Vsn}).

-spec get_all_pools() -> [pid()].
get_all_pools() ->
    ClusterState = get_state(),
    SlotsMapList = tuple_to_list(ClusterState#cluster_state.slots_maps),
    [SlotsMap#slots_map.node#node.pool || SlotsMap <- SlotsMapList,
        SlotsMap#slots_map.node =/= undefined].

-spec get_pool_by_slot(Slot:: integer()) ->
    {PoolName::atom() | undefined, Version::integer()}.
get_pool_by_slot(Slot) ->
    ClusterState = get_state(),
		case ClusterState#cluster_state.slots =/= {} andalso ClusterState#cluster_state.slots_maps =/= {} of
			true ->
				Index = element(Slot+1, ClusterState#cluster_state.slots),
				Cluster = element(Index, ClusterState#cluster_state.slots_maps),
				if
					Cluster#slots_map.node =/= undefined ->
						{Cluster#slots_map.node#node.pool, ClusterState#cluster_state.version};
					true ->
						{undefined, ClusterState#cluster_state.version}
				end;
			false ->
				{undefined, ClusterState#cluster_state.version}
		end.
%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
-spec(start_link() ->
	{ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
	{ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
	{stop, Reason :: term()} | ignore).
init([]) ->
	{ok, #state{}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
		State :: #state{}) ->
	{reply, Reply :: term(), NewState :: #state{}} |
	{reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
	{noreply, NewState :: #state{}} |
	{noreply, NewState :: #state{}, timeout() | hibernate} |
	{stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
	{stop, Reason :: term(), NewState :: #state{}}).

handle_call({reload_slots_map, Version}, _From, #state{version=Version} = State) ->
    {reply, ok, reload_slots_map(State)};
handle_call({reload_slots_map, _}, _From, State) ->
    {reply, {error, vsn_is_incorrect}, State};
handle_call(_Request, _From, State) ->
    {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
	{noreply, NewState :: #state{}} |
	{noreply, NewState :: #state{}, timeout() | hibernate} |
	{stop, Reason :: term(), NewState :: #state{}}).
handle_cast({load_slots_maps, Nodes, PoolName, PoolSize, MaxOFlowSize}, State0) ->
	{ok, cancel} = safe_timer_cancel(State0#state.tref_load_maps),
	State = run_monitors(State0#state{init_nodes = Nodes, pool_name = PoolName, pool_size = PoolSize, max_overflow_size = MaxOFlowSize}),
	State1 = reload_slots_map(State),
	State2 =
		case check_loaded_maps(State1) of
			{ok, all_loaded} ->
				State1;
			{error, Err} ->
				error_logger:error_msg("{redis cluster controller} check_load_maps e. = ~p~n", [Err]),
				{ok, Tref} = timer:send_after(5000, self(), load_slots_maps),
				State1#state{tref_load_maps = Tref}
		end,
	{noreply, State2};
handle_cast(_Request, State) ->
	{noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
	{noreply, NewState :: #state{}} |
	{noreply, NewState :: #state{}, timeout() | hibernate} |
	{stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
	{noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
		State :: #state{}) -> term()).
terminate(_Reason, _State) ->
	ok.

%%--------------------------------------------------------------------
%% @private
%% @docxww
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
		Extra :: term()) ->
	{ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
check_loaded_maps(#state{monitors = Monitors}) ->
    case [{Mon, St} || {Mon, St} <- Monitors, St =:= not_loaded] of
        [] ->
            {ok, all_loaded};
        NotLoaded -> {error, NotLoaded}
    end.

-spec get_state() -> #state{}.
get_state() ->
    [{cluster_state, ClState}] = ets:lookup(cluster_state_tab, cluster_state),
    ClState.


run_monitors(#state{init_nodes = InitNodes, monitors = []} = State) ->
    run_monitors(InitNodes, State);
run_monitors(State) ->
    State.

run_monitors([], State) -> State;
run_monitors([{Host, Port}|T], #state{monitors = Monitors} = State) ->
    MonitorName = eredis_cluster:get_name("MONITOR_", Host, Port),
    NewState =
        case supervisor:start_child(eredis_cluster_sup, ?CHILD(eredis_cluster_monitor, worker, [{MonitorName, Host, Port}])) of
            {ok, _Pid} ->
                State#state{monitors = [{MonitorName, not_ready}|Monitors]};
            {error,{already_started, _Pid}} ->
                State
        end,
    run_monitors(T, NewState).


-spec reload_slots_map(State::#state{}) -> NewState::#state{}.
reload_slots_map(#state{monitors = Monitors, slots_maps = SlotsMaps} = State0) ->
    error_logger:info_msg("{redis cluster controller} start reloading slot maps ~n", []),
    [close_connection(SlotsMap) || SlotsMap <- SlotsMaps],
    State = reload_slots_map(State0#state{slots_maps = []}, length(Monitors)),
    update_cluster_state(State).

reload_slots_map(NewState, 0) -> NewState;
reload_slots_map(#state{monitors = [{Monitor, _StateMonitor}|T], slots_maps = AccSlotsMaps, pool_name = PoolName, pool_size = PoolSize, max_overflow_size = MaxOFlowSize} = State, Len) ->
    case safe_call(Monitor)  of
        {ok, SlotsMaps} ->
						ConnectedSlotsMaps = connect_all_slots(SlotsMaps, PoolName, PoolSize, MaxOFlowSize),
            error_logger:info_msg("{redis cluster controller} ~p success got slot maps = ~p~n", [Monitor, ConnectedSlotsMaps]),
            reload_slots_map(State#state{monitors = [{Monitor, ok_loaded}|T], slots_maps = ConnectedSlotsMaps ++ AccSlotsMaps}, Len-1);
        {error, Error} ->
            error_logger:error_msg("{redis cluster controller} ~p e. get_slots_map = ~p~n", [Monitor, Error]),
            reload_slots_map(State#state{monitors = [{Monitor, not_loaded}|T]}, Len-1)
    end.

safe_call(Monitor) ->
    try
        eredis_cluster_monitor:get_cluster_slots(Monitor)
    catch
        _:Err  ->
            error_logger:error_msg("{redis cluster controller} ~p e. get_slots_map = ~p~n", [Monitor, Err]),
						{error, Err}
    end.

-spec connect_all_slots([#slots_map{}], atom(), integer(), integer()) -> [integer()].
connect_all_slots(SlotsMapList, PoolName, PoolSize, MaxOFlowSize) ->
    [SlotsMap#slots_map{node=connect_node(SlotsMap#slots_map.node, PoolName, PoolSize, MaxOFlowSize)} || SlotsMap <- SlotsMapList].

-spec connect_node(#node{}, atom(), integer(), integer()) -> #node{} | undefined.
connect_node(Node, PoolName, PoolSize, MaxOFlowSize) ->
    case eredis_cluster_pool:create(Node#node.address, Node#node.port, PoolName, PoolSize, MaxOFlowSize) of
        {ok, Pool} ->
            Node#node{pool=Pool};
        _ ->
            undefined
    end.

-spec close_connection(#slots_map{}) -> ok.
close_connection(SlotsMap) ->
    Node = SlotsMap#slots_map.node,
    if
        Node =/= undefined ->
            try eredis_cluster_pool:stop(Node#node.pool) of
                _ ->
                    ok
            catch
                _ ->
                    ok
            end;
        true ->
            ok
    end.


update_cluster_state(#state{version = Vsn, slots_maps = SlotsMaps} = State) ->
    Slots = create_slots_cache(SlotsMaps),
    NewClusterState = #cluster_state{
        slots = list_to_tuple(Slots),
        slots_maps = list_to_tuple(SlotsMaps),
        version = Vsn + 1
    },
    true = ets:insert(cluster_state_tab, [{cluster_state, NewClusterState}]),
    State#state{version = Vsn + 1}.


-spec create_slots_cache([#slots_map{}]) -> [integer()].
create_slots_cache(SlotsMaps) ->
    SlotsCache = [
        [{Index, SlotsMap#slots_map.index} ||
            Index <- lists:seq(SlotsMap#slots_map.start_slot, SlotsMap#slots_map.end_slot)]
        || SlotsMap <- SlotsMaps],

    SlotsCacheF = lists:flatten(SlotsCache),
    SortedSlotsCache = lists:sort(SlotsCacheF),
    [ Index || {_, Index} <- SortedSlotsCache].


safe_timer_cancel(undefined) -> {ok, cancel};
safe_timer_cancel({_, Ref} = TimerRef) when is_reference(Ref) ->
    timer:cancel(TimerRef).
