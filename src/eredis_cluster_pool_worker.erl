-module(eredis_cluster_pool_worker).

-behaviour(gen_server).
-behaviour(poolboy_worker).

%% API.
-export([start_link/1]).
-export([query/2]).

-define(RECONNECT_TIMEOUT, 5000).
-define(WAIT_AFTER_TRYING_RECONNECTS, 15000).
-define(RECONNECTION_LIMIT, 12).

-define(HEALTH_CHECK_TIMEOUT, 5000).

%% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-record(state, {
    state = disconnected :: disconnected | connected,
    ctrl_name = undefined :: unefined | pid() | module(),
    args = undefined :: unefined | [any()],
    connection_pid = undefined :: undefined | pid(),
    reconnec_tref = undefined :: undefined | reference(),
    reconnect_timeout = ?RECONNECT_TIMEOUT :: integer(),
    reconnect_counter = 0 :: integer(),
    health_check_tref = undefined :: undefined | reference()}).

%%%===================================================================
%%% API
%%%===================================================================
query(Worker, Commands) ->
    gen_server:call(Worker, {'query', Commands}).

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
-spec(start_link(term()) ->
    {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link(Args) ->
    gen_server:start_link(?MODULE, Args, []).


init(Args) ->
    process_flag(trap_exit, true),
    _Msg = erlang:send(self(), connect),
    {ok, #state{args = Args}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
    {reply, Reply :: term(), NewState :: #state{}} |
    {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_call({'query', [[X|_]|_] = Commands}, _From, #state{state = connected, connection_pid = Conn} = State)
    when is_list(X); is_binary(X) ->
    {reply, eredis:qp(Conn, Commands), State};
handle_call({'query', Command}, _From, #state{state = connected, connection_pid = Conn} = State) ->
    {reply, eredis:q(Conn, Command), State};
handle_call({'query', _}, _From, #state{state = disconnected} = State) ->
    {reply, {error, no_connection}, State};
handle_call(_Request, _From, State) ->
    {reply, unrecognized_msg, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_cast(_Request, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_info(connect, State) ->
    {ok, cancel} = safe_timer_cancel(State#state.reconnec_tref),
    NewState = connect(State),
    {noreply, NewState};
handle_info(check_healthy, #state{args = Args} = State) ->
    _ = safe_timer_cancel(State#state.health_check_tref),
    NewSt =
        try
            {ok, _SomeRes} = eredis:q(State#state.connection_pid, ["DEL", "check_health"]),
            {ok, Tref0} = timer:send_after(?HEALTH_CHECK_TIMEOUT, self(), check_healthy),
            State#state{state = connected, health_check_tref = Tref0}
        catch
            _:{_, ShortReason} ->
                error_logger:error_msg("{redis worker} connection to = ~p is not healthy with reason ~p~n", [Args, ShortReason]),
                catch eredis:stop(State#state.connection_pid),
                {ok, Tref} = timer:send_after(?RECONNECT_TIMEOUT, self(), connect),
                State#state{state = disconnected, reconnec_tref = Tref, reconnect_counter = State#state.reconnect_counter + 1};
            _:Reason ->
                error_logger:error_msg("{redis worker} connection to = ~p is not healthy with reason ~p~n", [Args, Reason]),
                catch eredis:stop(State#state.connection_pid),
                {ok, Tref} = timer:send_after(?RECONNECT_TIMEOUT, self(), connect),
                State#state{state = disconnected, reconnec_tref = Tref, reconnect_counter = State#state.reconnect_counter + 1}
        end,
    {noreply, NewSt};
%% need check if master was changed
handle_info(reconnection_limit, #state{} = State) ->
    error_logger:error_msg("{redis worker} ~p reconn limit = ~p~n", [self(), State#state.reconnect_counter]),
    _ = safe_timer_cancel(State#state.reconnec_tref),
    catch eredis:stop(State#state.connection_pid),
    {noreply, State#state{reconnec_tref = undefined, reconnect_counter = 0}};
%% Monitor Msg
handle_info({'EXIT', Pid, normal}, State) ->
    error_logger:error_msg("{redis worker} 'Exit' msg connect = ~p was broke normal ~n", [Pid]),
    catch eredis:stop(State#state.connection_pid),
    {ok, Tref} = timer:send_after(?RECONNECT_TIMEOUT, self(), connect),
    {noreply, State#state{reconnec_tref = Tref, reconnect_counter = 0}};
handle_info({'EXIT', Pid, Reason}, State) ->
    error_logger:error_msg("{redis worker} 'Exit' msg connect = ~p was broke with e. reason = ~p~n", [Pid, Reason]),
    {ok, Tref} = timer:send_after(?RECONNECT_TIMEOUT, self(), connect),
    {noreply, State#state{reconnec_tref = Tref, reconnect_counter = 0}};
handle_info({'DOWN', Ref, process, Pid, Reason}, State) ->
    erlang:demonitor(Ref),
    error_logger:error_msg("{redis worker} info 'Down' connect = ~p was broke reason = ~p~n", [Pid, Reason]),
    {ok, Tref} = timer:send_after(?RECONNECT_TIMEOUT, self(), connect),
    {noreply, State#state{reconnec_tref = Tref, reconnect_counter = 0}};
handle_info(_Info, State) ->
    error_logger:error_msg("{redis worker} unrecognized msg ~p~n", [_Info]),
    {noreply, State}.


-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(Reason, State) ->
    error_logger:error_msg("{redis worker} terminated with reason ~p~n", [Reason]),
    catch eredis:stop(State#state.connection_pid),
    ok.

-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
    {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%%%===================================================================
%%% Internal functions
%%%===================================================================
-spec connect(#state{}) -> #state{}.
connect(#state{args = Args} = State) when State#state.reconnect_counter < ?RECONNECTION_LIMIT ->
    Host = proplists:get_value(host, Args),
    Port = proplists:get_value(port, Args),
    try
        {ok, Conn} = eredis:start_link(Host, Port),
        error_logger:info_msg("{redis worker} connected to ~p~n", [{Host, Port}]),
        erlang:monitor(process, Conn),
        {ok, Tref0} = timer:send_after(?HEALTH_CHECK_TIMEOUT, self(), check_healthy),
        State#state{state = connected, health_check_tref = Tref0, connection_pid = Conn, reconnect_counter = 0}
    catch _:Reason ->
        error_logger:error_msg("{redis worker} cannot connect to ~p e. ~p reconnect try ~p~n",
            [{Host, Port}, Reason, State#state.reconnect_counter]),
        {ok, Tref} = timer:send_after(?RECONNECT_TIMEOUT, self(), connect),
        State#state{state = disconnected, reconnec_tref = Tref, reconnect_counter = State#state.reconnect_counter + 1}
    end;
connect(State) ->
    erlang:send(self(), reconnection_limit),
    State.

safe_timer_cancel(undefined) -> {ok, cancel};
safe_timer_cancel({_, Ref} = TimerRef) when is_reference(Ref) ->
    timer:cancel(TimerRef).
