-module(eredis_cluster_sup).
-behaviour(supervisor).

%% Supervisor.
-export([start_link/0]).
-export([init/1]).

-include("eredis_cluster.hrl").
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).


-spec start_link() -> {ok, pid()}.
start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

-spec init([])
	-> {ok, {{supervisor:strategy(), 1, 5}, [supervisor:child_spec()]}}.
init([]) ->
    ok = create_tabs(),
    {ok, {{one_for_one, 5, 10}, [
        ?CHILD(eredis_cluster_pool, supervisor),
        ?CHILD(eredis_cluster_controller, worker)
    ]
    }}.

create_tabs() ->
    _Tab = ets:new(cluster_state_tab, [public, set, named_table, {read_concurrency, true}]),
    true = ets:insert(cluster_state_tab, {cluster_state, #cluster_state{}}),
    ok.
